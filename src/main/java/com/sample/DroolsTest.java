package com.sample;

import org.kie.api.KieServices;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.text.SimpleDateFormat;

import org.drools.core.event.DebugAgendaEventListener;
import org.drools.core.event.DebugRuleRuntimeEventListener;

/**
 * This is a sample class to launch a rule.
 */
@SuppressWarnings("restriction")
public class DroolsTest {

    public static final void main(String[] args) {
        try {
	        KieServices ks = KieServices.Factory.get();
    	    KieContainer kContainer = ks.getKieClasspathContainer();
        	KieSession kSession = kContainer.newKieSession("ksession-rules");

        	kSession.addEventListener(new DebugAgendaEventListener());
        	kSession.addEventListener(new DebugRuleRuntimeEventListener());

        	KieRuntimeLogger logger = ks.getLoggers().newFileLogger(kSession, "./casoemergencias.log");
        	
        	Institucion hospitalCentral, hospitalNotti;
        	hospitalCentral = new Institucion();
        	hospitalCentral.setId(1);
        	hospitalCentral.setDescripcion("HOSPITAL CENTRAL");
        	hospitalNotti = new Institucion();
        	hospitalNotti.setId(1);
        	hospitalNotti.setDescripcion("HOSPITAL NOTTI");
        	kSession.insert(hospitalCentral);
        	kSession.insert(hospitalNotti);
        	
        	Codigo rojo, amarillo, verde, azul, blanco;
        	rojo = new Codigo();
        	rojo.setId(1);
        	rojo.setCodigo("RJO");
        	rojo.setDescripcion("rojo");
        	amarillo = new Codigo();
        	amarillo.setId(1);
        	amarillo.setCodigo("AMA");
        	amarillo.setDescripcion("amarillo");
        	verde = new Codigo();
        	verde.setId(1);
        	verde.setCodigo("VDE");
        	verde.setDescripcion("verde");
        	azul = new Codigo();
        	azul.setId(1);
        	azul.setCodigo("AZU");
        	azul.setDescripcion("azul");
        	blanco = new Codigo();
        	blanco.setId(1);
        	blanco.setCodigo("BLA");
        	blanco.setDescripcion("blanco");
        	kSession.insert(rojo);
        	kSession.insert(amarillo);
        	kSession.insert(verde);
        	kSession.insert(azul);
        	kSession.insert(blanco);
        	
        	MotivoLlamado rojoGenerico, amarilloGenerico, hemodigalta;
        	rojoGenerico = new MotivoLlamado();
        	rojoGenerico.setTipo(rojo);
        	rojoGenerico.setDescripcion("CODIGO ROJO GENERICO");
        	amarilloGenerico = new MotivoLlamado();
        	amarilloGenerico.setTipo(amarillo);
        	amarilloGenerico.setDescripcion("CODIGO AMARILLO GENERICO");
        	hemodigalta = new MotivoLlamado();
        	hemodigalta.setTipo(rojo);
        	hemodigalta.setDescripcion("HEMORRAGIA DIGESTIVA ALTA");
        	kSession.insert(rojoGenerico);
        	kSession.insert(amarilloGenerico);
        	kSession.insert(hemodigalta);
        	
        	Paciente adulto, nino;
        	adulto = new Paciente();
        	adulto.setEdad(35);
        	nino = new Paciente();
        	nino.setEdad(8);
        	kSession.insert(adulto);
        	kSession.insert(nino);
        	
        	SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        	
        	Despacho demoraLlegada, demoraSalida;
        	demoraLlegada = new Despacho();
        	demoraLlegada.setFecha(format.parse("00:00:01"));
        	demoraLlegada.setHoradisp(format.parse("12:35:00"));
        	demoraLlegada.setHllamado(format.parse("12:00:00"));
        	demoraLlegada.setHdespacho(format.parse("12:30:00"));
        	demoraLlegada.setHllegadadom(format.parse("12:35:00"));
        	demoraSalida = new Despacho();
        	demoraSalida.setFecha(format.parse("00:00:01"));
        	demoraSalida.setHoradisp(format.parse("17:10:00"));
        	demoraSalida.setHdespacho(format.parse("17:40:00"));
        	demoraSalida.setHllamado(format.parse("17:05:00"));
        	demoraSalida.setHllegadadom(format.parse("17:45:00"));
        	kSession.insert(demoraLlegada);
        	kSession.insert(demoraSalida);
        	
        	Recepcion caso1 = new Recepcion();
        	caso1.setPaciente(adulto);
        	caso1.setMotivo(amarilloGenerico);
        	caso1.setDespacho(demoraSalida);
        	kSession.insert(caso1);
        	
        	Recepcion caso2 = new Recepcion();
        	caso2.setPaciente(nino);
        	caso2.setMotivo(hemodigalta);
        	caso2.setDespacho(demoraLlegada);
        	kSession.insert(caso2);
        	
        	kSession.fireAllRules();
            
            kSession.dispose();
            logger.close();
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}

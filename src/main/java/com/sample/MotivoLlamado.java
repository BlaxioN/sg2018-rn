package com.sample;

public class MotivoLlamado {
	Codigo tipo;
	String descripcion;
	public Codigo getTipo() {
		return tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setTipo(Codigo tipo) {
		this.tipo = tipo;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}

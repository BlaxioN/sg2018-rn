package com.sample;

public class Recepcion {
	Despacho despacho;
	MotivoLlamado motivo;
	Paciente paciente;
	public Despacho getDespacho() {
		return despacho;
	}
	public MotivoLlamado getMotivo() {
		return motivo;
	}
	public Paciente getPaciente() {
		return paciente;
	}
	public void setDespacho(Despacho despacho) {
		this.despacho = despacho;
	}
	public void setMotivo(MotivoLlamado motivo) {
		this.motivo = motivo;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
}

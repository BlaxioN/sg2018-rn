package com.sample;

import java.util.Date;

public class Apercibimiento {
	String motivo;
	Date fecha;
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getMotivo() {
		return motivo;
	}
	public Date getFecha() {
		return fecha;
	}
}

package com.sample;

public class Movil {
	int base;
	String posicion;
	int activo;
	public int getBase() {
		return base;
	}
	public String getPosicion() {
		return posicion;
	}
	public int getActivo() {
		return activo;
	}
	public void setBase(int base) {
		this.base = base;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
}
